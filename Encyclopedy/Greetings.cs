﻿using System;

using System.Windows.Forms;

namespace Encyclopedy
{
    public partial class Greetings : Form
    {
        public Greetings()
        {
            InitializeComponent();
            Timer t = new Timer();
            t.Interval = 6000;
            t.Start();
            t.Tick += new EventHandler(t_Tick);
            timer1.Start();

            Opacity = 0;
            Timer timer = new Timer();
            timer.Tick += new EventHandler((sender, e) =>
            {
                if((Opacity += 0.005d) == 1)
                    timer.Stop();
            });

            timer.Interval = 1;
            timer.Start();
        }

        private void t_Tick(object sender, EventArgs e)
        {
            Close();
        }

   
    }
}
