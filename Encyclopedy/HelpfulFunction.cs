﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System;
using System.Drawing;

namespace Encyclopedy
{  // статичний клас для полегшення роботи та збереження необхідних даних
    static class HelpfulFunction
    {
        private static string LastLineFromFile { get; set; } // викор для віновлення текста із файла

        public static bool DataIsSaved { get; set; } // сигналізує про збереження усіх даних
        public static TitleData SelectedTitleData { get; set; } // збереження обраної статті для відораження
        private static List<FontTypeData> listAllChangeFonts = new List<FontTypeData>(); //збереження усіх змін фонів

        public static List<FontTypeData> ListAllChangeFonts 
        {
            get { return listAllChangeFonts; }
            set { listAllChangeFonts = value; }
        }
       
        // отримання типу шрифта від імені
        public static FontStyle GetFontStyleFromString(string fontStyle)
        {
            switch (fontStyle)
            {
                case "Bold":
                    {
                         return   FontStyle.Bold;
                    }
                case "Italic":
                    {
                         return   FontStyle.Italic;
                    }
                case "Underline":
                    {
                         return   FontStyle.Underline;
                    }
                default:
                    {
                        return FontStyle.Regular;
                    }
            }
        }
        // функція відновлення змісту файла у текст
        public static void RecoverArticleFromFile(/*StreamReader strRead*/string strRead, RichTextBox itemText)
        {
            int length = "changeFont###".Length;
            int indexStart = strRead.IndexOf("changeFont###");
                if (indexStart != -1)
                {
                    int indexEnd = strRead.IndexOf("$$$");
                    string result = strRead.Substring(indexStart + length, indexEnd - length);
                    string[] typeArray = result.Split(',');
                    if (typeArray.Length >= 5) // отримання виду та типу шрифта та відновлення їх
                    {
                        itemText.SelectionFont = new Font(typeArray[0], float.Parse(typeArray[1]),
                                    GetFontStyleFromString(typeArray[2]));
                        itemText.SelectionColor = Color.FromName(typeArray[3]);
                        listAllChangeFonts.Add(new FontTypeData()
                        {
                            FontName = typeArray[0],
                            FontSize = typeArray[1],
                            FontType = typeArray[2],
                            ColorName = typeArray[3],
                            Position = int.Parse(typeArray[4])
                        }
                            );
                    }
                    result = strRead.Substring(indexEnd + 3);
                    indexEnd = result.IndexOf("changeFont###");
                    if (indexEnd >= 0)
                    {
                        itemText.SelectedText += result.Substring(0, indexEnd);
                        RecoverArticleFromFile(result.Substring(indexEnd), itemText);
                    }
                    else
                    {
                        itemText.SelectedText += result;
                    }

                }
        }

        // статичний метод збереження даних у файл із всіма шрифтами
        public static void SaveArticleInFile(string allText, StreamWriter strWriter)
        {
            RemoveDuplicatesFromFonts();
            for (int i = 0; i < listAllChangeFonts.Count;i++ )
            {
                if (listAllChangeFonts[i].Position >= allText.Length)
                    listAllChangeFonts.RemoveAt(i);
            }

            FontTypeData[] sortedArray = listAllChangeFonts.ToArray();
            Array.Sort(sortedArray);
            for (int i = 0; i < sortedArray.Length; i++)
            {
                strWriter.Write("changeFont###");
                strWriter.Write(sortedArray[i].FontName + "," + sortedArray[i].FontSize + "," + sortedArray[i].FontType+"," + sortedArray[i].ColorName + "," + sortedArray[i].Position+"$$$");
                if ((i + 1) >= sortedArray.Length)
                {
                    if(sortedArray[i].Position < allText.Length)
                        strWriter.Write(allText.Substring(sortedArray[i].Position));
                }
                else
                {
                    int length = sortedArray[i + 1].Position - sortedArray[i].Position;//?????? need check if need -1
                    strWriter.Write(allText.Substring(sortedArray[i].Position, length));
                }
            }
        }

        // отримання нових даних про шрифт під час зміни рифту через виділення
        public static FontTypeData GetNextFontTypeInsertedText()
        {
            FontTypeData last = listAllChangeFonts[listAllChangeFonts.Count-1];
            RemoveDuplicatesFromFonts();
            int minValue = last.Position - listAllChangeFonts[0].Position;
            int minIndex = 0;
            for (int i = 0; i < listAllChangeFonts.Count; i++)
            {
                if (listAllChangeFonts[i].Position >= last.Position)
                    continue;
                int sub = last.Position - listAllChangeFonts[i].Position;
                if (sub < minValue)
                {
                    minValue = sub;
                    minIndex = i;
                }
            }
            return listAllChangeFonts[minIndex];

        }
        // функція видалення дублікатів шрифтів з однаковою позицією. приймається останній варіант
        private static void RemoveDuplicatesFromFonts()
        {
            for (int i = 0; i < listAllChangeFonts.Count; i++)
            {
                for (int j = i + 1; j < listAllChangeFonts.Count; j++)
                {
                    if (listAllChangeFonts[i].Position == listAllChangeFonts[j].Position)
                    {
                        ListAllChangeFonts.RemoveAt(i);//????   j was
                        
                    }
                }
            }
        }

        // відновлення дерева розділів та підрозділів з файл
        public static void TreeViewListAdd(TreeView treeView1)
        {
            string pathHeaderFile = Application.StartupPath;
            pathHeaderFile += @"\Configuration";
            if (Directory.Exists(pathHeaderFile))
            {
                if (File.Exists(pathHeaderFile + @"\headers.txt"))
                {
                    StreamReader strReader = new StreamReader(@pathHeaderFile + @"\headers.txt");
                    TreeNode root = new TreeNode() { Name = "1", Text = "Зміст" };
                    treeView1.Nodes.Add(BuildViewTreeFromFile(ref strReader, new TreeNode() { Name = root.Name, Text = root.Text }));
                    strReader.Close();
                }
                else
                {
                    treeView1.Nodes.Add(new TreeNode() { Name = "1", Text = "Зміст" });
                }

            }
            else
                treeView1.Nodes.Add(new TreeNode() { Name = "1", Text = "Зміст" });
        }

        // збереження дерева розділів та підрозділів у файл
        public static void WriteAllTreeInFile(StreamWriter swWriter, TreeNode treeNode)
        {
            foreach (TreeNode VARIABLE in treeNode.Nodes)
            {
                swWriter.WriteLine(VARIABLE.Name + " " + VARIABLE.Text);
                WriteAllTreeInFile(swWriter, VARIABLE);
            }
        }

        // визначення нового імені для створеного підрозділу
        public static string GetNewNameNode(TreeNode selectNode)
        {
            string name = selectNode.Name;
            if (selectNode.Nodes.Count == 0)
                name += "-1";
            else
            {
                string lastNumber = selectNode.Nodes[selectNode.Nodes.Count-1].Name;
                lastNumber = lastNumber.Substring(lastNumber.LastIndexOf("-")+1);
                name += "-" + (int.Parse(lastNumber) + 1).ToString();
                //name += "-" + (selectNode.Nodes.Count+1).ToString();
            }
            return name;
        }


        //функція відновлення дерева розділів із файлу
        public static TreeNode BuildViewTreeFromFile(ref StreamReader strRead, TreeNode parent, bool readFromFile = true)
        {
            
            //TreeNode result = new TreeNode();
            string startNode = parent.Name;
            if (!strRead.EndOfStream || !readFromFile)
            {
                string line;
                
                if (readFromFile)
                {
                    line = strRead.ReadLine();
                    LastLineFromFile = line;
                }
                else
                    line = LastLineFromFile;
                string[] arrayLine = new string[2];
                arrayLine[0] = line.Substring(0, line.IndexOf(" "));
                arrayLine[1] = line.Substring(line.IndexOf(" ") + 1);
                if (startNode.Length < arrayLine[0].Length)
                {
                    TreeNode child = new TreeNode() { Name = arrayLine[0], Text = arrayLine[1] };
                    parent.Nodes.Add(child);
                    BuildViewTreeFromFile(ref strRead, parent.Nodes[parent.Nodes.IndexOfKey(child.Name)]);
                }
                else 
                {
                   
                    BuildViewTreeFromFile(ref strRead, parent.Parent, false);
                }
                    
            }

            return parent;
        }
    }
}
