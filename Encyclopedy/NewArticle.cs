﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace Encyclopedy
{
    public partial class NewArticle : Form
    {
        public NewArticle()
        {
            InitializeComponent();
           
        }

        public string Action { get; set; } // збереження дії: створення, редагування, видалення
        private string FontStyleSave { get; set; } // вид шрифту: курсив, жирний, підкреслений
        private int itemLastLength { get; set; } // визначення переміщення курсора у полі тесту
        private bool FirstStart { get; set; } // перший запуск програми

 
        private void NewArticle_Load(object sender, EventArgs e)
        {

          
            FirstStart = true;
            ClearAllComponents();


            HelpfulFunction.TreeViewListAdd(treeView1); // відновлення дерева католога
            AddShiftSize(); // додавання розмірів шрифтів
            AddShifts(); // додавання шрифтів
        
            toolStripShifts.DropDownStyle = ComboBoxStyle.DropDownList;
           
            HelpfulFunction.ListAllChangeFonts.Clear();
            FontStyleSave = "Regular";

            itemText.Font = itemText.SelectionFont = new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text));
        
            switch (Action)
            {
                case "Create":
                {
                   this.Tag = "Додавання нової статті до розділу: ";
                   AddFontToArray();
                   itemText.Font = itemText.SelectionFont = new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text));
        
                    break;
                }
                case "Delete":
                case "Replace":
          
                {
                    textBox1.Enabled = false;
                    if (HelpfulFunction.SelectedTitleData != null)
                    {
                        LoadComponentFromTitleData();
                        foreach (TreeNode VARIABLE in treeView1.Nodes)
                        {
                            SelectDeleteTitleData(VARIABLE);
                        }
                        
                    }
                    this.Tag = "Редагування статті: ";
                    break;
                }
            }
            if (Action == "Delete")
            {
                button1.Text = "Видалити";
                itemText.Enabled = false;
                Tag = "Видалення статті: ";
            }
            FirstStart = false;
          //  itemText.Font= itemText.SelectionFont = new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text));
        
        }


        // створення нового розділу у змісті
        private void створитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            if (!CheckSelectedNode("для якого бажаєте створити підрозділ"))
                return;

            Redaction red = new Redaction();
            red.LabelText = "Введіть нову назву розділу (підрозділу)";
            red.Text = "Створення нового розділу";
            if (red.ShowDialog() == DialogResult.OK)
            {
                treeView1.SelectedNode.Nodes.Add(new TreeNode()
                {
                    Name = HelpfulFunction.GetNewNameNode(treeView1.SelectedNode),
                    Text = red.Result.Trim()
                }
                );
                string path = Application.StartupPath + @"\Configuration\";
                WriteTreeNodeInFile(path.Substring(0, path.LastIndexOf(@"\")));
                
            }
            HelpfulFunction.DataIsSaved = true;
               
            
        }
        // функція перейменування розділу у дереві каталогу
        private void перейменуватиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckSelectedNode("який бажаєте перейменувати"))
                return;

            Redaction red = new Redaction();
            red.LabelText = "Введіть нову назву для обраного розділу";
            red.Text = "Перейменування розділу";
            red.Result = treeView1.SelectedNode.Text;
            string lastName = red.Result;

            if (red.ShowDialog() == DialogResult.OK)
            { // внесення нового імені до файлу
                treeView1.SelectedNode.Text = red.Result.Trim();
                string path = Application.StartupPath + @"\Configuration\Titles.xml";
                XmlTitleReadWrite xmlTitle = new XmlTitleReadWrite(@path);
                xmlTitle.RenameTitle(lastName, red.Result.Trim());
                WriteTreeNodeInFile(path.Substring(0, path.LastIndexOf(@"\")));
                if( lastName == textBox1.Text)
                    textBox1.Text = red.Result.Trim();

            }
            HelpfulFunction.DataIsSaved = true;
        }
        //функція збереження даних
        private void button1_Click(object sender, EventArgs e)
        {
            if (Action == "Delete")
            {
                видалитиToolStripMenuItem_Click(new object(), new EventArgs());
                if (
                    MessageBox.Show(this, "Дані успішно видалені.\n Бажаєте видалити ще дані?", "Інформація",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    HelpfulFunction.SelectedTitleData = null;
                    NewArticle_Load(new object(), new EventArgs());
                }
                else
                {
                    Close();
                }
                return;
            }
            if (IsEmptyComponent())
                return;
            
            string title = HelpfulFunction.GetNewNameNode(treeView1.SelectedNode);
            string path = Application.StartupPath;
            path += @"\Configuration";
            if (!Directory.Exists(@path))
                Directory.CreateDirectory(@path);

            if(SaveTitleAndDataInFile(path, title) == false)
                return;
            // save all headers in treeview 
            if (Action == "Create")
            {

                treeView1.SelectedNode.Nodes.Add(new TreeNode()
                {
                    Name = title,
                    Text = textBox1.Text
                }
                    );
            }
            
            WriteTreeNodeInFile(path);
            // save new title in file
            itemText.Clear();
            textBox1.Clear();
            treeView1.Refresh();
            addrImages.Clear();
            
            HelpfulFunction.DataIsSaved = true;
            if (
                MessageBox.Show(this, "Дані успішно збережено!!!\n Бажаєте створити (редагувати) нову статтю?", "Інформація",
                    MessageBoxButtons.YesNo) == DialogResult.No)
            {
                this.Close();
               
            }
        
                itemText.SelectionFont = new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text));
         
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
         
            HelpfulFunction.DataIsSaved = true;
           this.Close();
        }

        // функція отримання нового кольору
        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                toolStripColor.BackColor = colorDialog1.Color;
                
                itemText.SelectionColor = colorDialog1.Color;
                AddFontToArray();
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
           ChangeFontStyles(toolStripButton3);
        }

        //функція зміни шрифту 
        private void ChangeFontStyles(ToolStripButton button)
        {
            if (button.BackColor == SystemColors.Control)
            {
                switch (button.Name)
                {
                    case "toolStripButton3":
                    {
                        FontStyleSave = "Underline";
                        itemText.SelectionFont = ChangeFonts(/*FontStyle.Underline*/);
                        break;
                    }
                    case "toolStripButton1":
                    {
                        FontStyleSave = "Bold";
                        itemText.SelectionFont = ChangeFonts(/*FontStyle.Bold*/);
                        break;
                    }
                    case "toolStripButton2":
                    {
                        FontStyleSave = "Italic";
                        itemText.SelectionFont = ChangeFonts(/*FontStyle.Italic*/);
                        break;
                    }

                }
                
            }
            else
            {
                button.BackColor = SystemColors.Control;
                FontStyleSave = "Regular";
                itemText.SelectionFont = ChangeFonts(/*FontStyle.Regular*/);
            }
        }

      

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ChangeFontStyles(toolStripButton2);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ChangeFontStyles(toolStripButton1);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.BackColor == Color.Red)
                textBox1.BackColor = SystemColors.Window;
            HelpfulFunction.DataIsSaved = false;
        }

        private void itemText_TextChanged(object sender, EventArgs e)
        {
           

            if (itemText.BackColor == Color.Red)
                itemText.BackColor = SystemColors.Window;
           

            HelpfulFunction.DataIsSaved = false;
            // перевірка додавання певного тексту всередину існуючого
            if ( FirstStart == false && itemText.SelectionStart <= itemText.Text.Length)
            {
                if (itemLastLength < itemText.Text.Length)
                {
                    RecountNewPositionOfFonts(true);
                }
                else if(itemLastLength > itemText.Text.Length)
                    RecountNewPositionOfFonts(false);
                itemLastLength = itemText.Text.Length;
            }
            
        }

        private void NewArticle_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Action != "Delete")
            {
                if (!HelpfulFunction.DataIsSaved)
                {
                    if (MessageBox.Show(this, "Дані були змінені, але не збережені. Зберегти зміни?", "інформація", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        button1_Click(new object(), new EventArgs());
                    }

                }
            }
            this.DialogResult = DialogResult.OK;
        }
        // функція видалення розділу та всіх його складових
        private void видалитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckSelectedNode("який бажаєте видалити"))
                return;

            if (MessageBox.Show(this, "Ви дійсно бажаєте видалити даний вузол (статтю)?\n Разом з ним будуть видалена вся інформація що відноситься до нього", "Інформація", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {  // внесення змін до конфігуруючих файлів
                string path = Application.StartupPath + @"\Configuration\Titles.xml";
                XmlTitleReadWrite xml = new XmlTitleReadWrite(path);
                xml.RemoveAllTitlesInTreeNode(treeView1.SelectedNode);
                treeView1.Nodes.Remove(treeView1.SelectedNode);
                treeView1.Refresh();
                WriteTreeNodeInFile(path.Substring(0, path.LastIndexOf(@"\")));
            }
            HelpfulFunction.DataIsSaved = true;

        }
        //функція відновлення звичайного шрифту для обраного розділу у дереві
        private void RecoverFontRegularInNode(TreeNode node)
        {
            foreach (TreeNode value in node.Nodes)
            {
                if (value.Name == lastSelectedName)
                {
                    value.NodeFont = new Font(treeView1.Font.FontFamily, treeView1.Font.Size,FontStyle.Regular);
                    return;
                }
                RecoverFontRegularInNode(value);
            }
        }

        
        string lastSelectedName = null;
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode == null)
                    return;

            if (lastSelectedName != null)
            {
                    RecoverFontRegularInNode(treeView1.TopNode);
            }
            if (treeView1.SelectedNode != treeView1.TopNode)
            {
                
                treeView1.SelectedNode.NodeFont = new Font(treeView1.Font.FontFamily, treeView1.Font.Size, FontStyle.Bold);
                
                treeView1.SelectedNode.Text = treeView1.SelectedNode.Text; // upgrade width of treeNode 
                lastSelectedName = treeView1.SelectedNode.Name;
            }
            if (this.Action != "Create")
            {
                

            string articleName = treeView1.SelectedNode.Text;
            string path = Application.StartupPath + @"\Configuration\Titles.xml";
            XmlTitleReadWrite xml = new XmlTitleReadWrite(path);
            string fileOpen = xml.GetFileNameOfArticle(articleName);
            HelpfulFunction.SelectedTitleData = xml.SelectedTitleData;
            LoadComponentFromTitleData();
            if (HelpfulFunction.SelectedTitleData == null)
            {
                textBox1.Text = articleName;
                itemText.Clear();
            }
            }
            this.Text = this.Tag + treeView1.SelectedNode.Text;
           

        }

        private void toolStripShiftSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            itemText.SelectionFont =  ChangeFonts(/*FontStyle.Regular*/);
            AddFontToArray();
            itemText.Focus();
        }

        private void toolStripShifts_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                itemText.SelectionFont = new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text));
                AddFontToArray();

            }
            catch (Exception ex)
            {
                ;
            }
            itemText.Focus();
        }


  #region Additionfunc


        // функція відновлення змісту статті та ключових слів: викор під час редагування\видалення
        private void LoadComponentFromTitleData()
        {
            if (HelpfulFunction.SelectedTitleData == null)
                return;
            textBox1.Text = HelpfulFunction.SelectedTitleData.Keywords;
            string path = Application.StartupPath + @"\Configuration\Articles\" +
                          HelpfulFunction.SelectedTitleData.FileName;
            if (File.Exists(path))
            {
                StreamReader streamReader = new StreamReader(@path);
                itemText.Clear();
                HelpfulFunction.RecoverArticleFromFile(streamReader.ReadToEnd(), itemText);
               
                itemLastLength = itemText.Text.Length;
                streamReader.Close();
            }
        }
        // функція визначення виду шрифта у полі введення інфо
        private void ChangeCursorOrSelectionText()
        {
            Font selection = itemText.SelectionFont;
            if (selection == null)
                selection = new System.Drawing.Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text),HelpfulFunction.GetFontStyleFromString(FontStyleSave) );

            string selectionFontSize = selection.Size.ToString();
         
            for (int i = 0; i < toolStripShiftSize.Items.Count;i++)
            {
                if (toolStripShiftSize.Items[i].ToString() == selectionFontSize)
                {
                    toolStripShiftSize.SelectedIndex = i;
                    break;
                }
            }
            selectionFontSize = selection.FontFamily.Name;
            for (int i = 0; i < toolStripShifts.Items.Count; i++)
            {
                if (toolStripShifts.Items[i].ToString() == selectionFontSize)
                {
                    toolStripShifts.SelectedIndex = i;
                    break;
                }
            }
            
            if (selection.Bold)
            {
                toolStripButton1.BackColor = SystemColors.ActiveBorder;
                toolStripButton2.BackColor = toolStripButton3.BackColor = SystemColors.Control;
                FontStyleSave = "Bold";
            }
            else if(selection.Italic)
            {
                toolStripButton2.BackColor = SystemColors.ActiveBorder;
                toolStripButton1.BackColor = toolStripButton3.BackColor = SystemColors.Control;
                FontStyleSave = "Italic";
            }
            else if(selection.Underline)
            {
                
                toolStripButton3.BackColor = SystemColors.ActiveBorder;
                toolStripButton1.BackColor = toolStripButton2.BackColor = SystemColors.Control;
                FontStyleSave = "Underline";
            }
            else
            {
                toolStripButton1.BackColor = toolStripButton2.BackColor = toolStripButton3.BackColor = SystemColors.Control;
                FontStyleSave = "Regular";
            }
            toolStripColor.BackColor = itemText.SelectionColor;

            
        }
        // функція перерахунку нових позицій шрифтів під час внесення інфо в середину
        private void RecountNewPositionOfFonts(bool lengthUp)
        {
            for (int i = 0; i < HelpfulFunction.ListAllChangeFonts.Count;i++)
            {

                if (itemText.SelectionStart <= HelpfulFunction.ListAllChangeFonts[i].Position)
                {
                    if(lengthUp)
                        HelpfulFunction.ListAllChangeFonts[i].Position += (itemText.Text.Length - itemLastLength);
                    else
                    {
                        HelpfulFunction.ListAllChangeFonts[i].Position -= (itemLastLength - itemText.Text.Length);
                        if (HelpfulFunction.ListAllChangeFonts[i].Position < 0)
                            HelpfulFunction.ListAllChangeFonts[i].Position = 0;
                    }
                }

            }
        }

        // пошук та виділення у дереві розділу який буде видалятися чи редагуватися 
        private void SelectDeleteTitleData(TreeNode node)
        {
            foreach (TreeNode VARIABLE in node.Nodes)
            {
                if (VARIABLE.Name == HelpfulFunction.SelectedTitleData.HeadId)
                {
                    treeView1.SelectedNode = VARIABLE;
                    return;
                }
                SelectDeleteTitleData(VARIABLE);
            }
        }
        // функція очистки всіх компонентів
        private void ClearAllComponents()
        {
            HelpfulFunction.DataIsSaved = true;
            treeView1.Nodes.Clear();

            textBox1.BackColor = itemText.BackColor = SystemColors.Window;
            textBox1.Clear();
            itemText.Clear();
            toolStripShifts.Items.Clear();
            toolStripShiftSize.Items.Clear();
            toolStripColor.BackColor = Color.Black;
            toolStripButton1.BackColor = toolStripButton2.BackColor = toolStripButton3.BackColor = SystemColors.Control;
        }
        // функція визначення усіх шрифтів встіновлених на компютері
        private void AddShifts()
        {
            System.Drawing.Text.InstalledFontCollection fonts = new System.Drawing.Text.InstalledFontCollection();
            foreach (var VARIABLE in fonts.Families)
            {
                toolStripShifts.Items.Add(VARIABLE.Name);
              
            }
            toolStripShifts.Text = toolStripShifts.Items[0].ToString();
        }

        private void AddShiftSize()
        {
            for (int i = 8; i < 48; i += 2)
                toolStripShiftSize.Items.Add(i.ToString());
            toolStripShiftSize.Text = "8";

        }
        // перевірка виділення певного розділа
        private bool CheckSelectedNode(string action)
        {
            if (treeView1.SelectedNode == null)
            {
                MessageBox.Show("Будь-ласка, оберіть розділ " + action);
                return false;
            }
            return true;
        }
        // збереження тексту статті у файл
        private bool SaveTitleAndDataInFile(string path, string title)
        {
            XmlTitleReadWrite saveTitle = new XmlTitleReadWrite(@path + @"\Titles.xml");
            path += @"\Articles";
            if (!Directory.Exists(@path))
                Directory.CreateDirectory(@path);
        
            if (Action == "Create")
            {
                List<string> keywords = saveTitle.GetAllKeywords();
                foreach (string value in keywords)
                {
                
                    if (value == textBox1.Text.Trim())
                    {
                        MessageBox.Show(
                            "Стаття із такою назвою або з такими ключовими словами уже існує\n Будь-ласка, введіть іншу назву");
                        textBox1.BackColor = Color.Red;
                        return false;
                    }
                }

                saveTitle.Write(new TitleData() { TitleId = 1, HeadId = title, Keywords = textBox1.Text, FileName = textBox1.Text + ".txt" });
                path += @"\" + textBox1.Text + ".txt";
               
            }
            else ///REplace action
            {
                saveTitle.RenameTitle(HelpfulFunction.SelectedTitleData.Keywords, textBox1.Text.Trim());
                path += @"\" + HelpfulFunction.SelectedTitleData.FileName;
                HelpfulFunction.SelectedTitleData.Keywords = textBox1.Text;
             
            }
            StreamWriter swWriter = new StreamWriter(@path);
            HelpfulFunction.SaveArticleInFile(itemText.Text, swWriter);
            HelpfulFunction.ListAllChangeFonts.Clear();
            // ????  що видаляється за директорія ?????
            if (Directory.Exists(@path))
            {
                Directory.Delete(@path);
            }
            else
            {// копіювання зображень у каталог до статті
                if (addrImages.Count != 0)
                {
                    path = path.Substring(0, path.LastIndexOf(@"\")+1);
                    path += HelpfulFunction.SelectedTitleData.TitleId;
                    if(!Directory.Exists(@path))
                        Directory.CreateDirectory(@path);
                    foreach (string value in addrImages)
                    {
                        string imageName = value.Substring(value.LastIndexOf(@"\") + 1);
                        File.Copy(value, @path + @"\" + imageName);
                    }
                }
            }

            swWriter.Close();
            return true;
        }
        //функція перевірки заповнення усіх полів
        private bool IsEmptyComponent()
        {
            if (treeView1.SelectedNode == null)
            {
                MessageBox.Show("Будь-ласка, оберіть розділ до якого необхідно віднести дану статтю");
                return true;
            }
            if (textBox1.Text == String.Empty)
            {
                MessageBox.Show("Будь-ласка, введіть заголовок для статті");
                textBox1.BackColor = Color.Red;
                return true;
            }
            if (itemText.Text == String.Empty)
            {
                MessageBox.Show("Будь-ласка, заповніть поле для статті відповідним текстом");
                itemText.BackColor = Color.Red;
                return true;
            }
            return false;
        }
        // функція запису дерева каталога у файл
        private void WriteTreeNodeInFile(string path)
        {
            string fullPath = @path + @"\headers.txt";

            StreamWriter swWriter = new StreamWriter(@fullPath);
            foreach (TreeNode VARIABLE in treeView1.Nodes)
            {
                HelpfulFunction.WriteAllTreeInFile(swWriter, VARIABLE);
            }
            swWriter.Flush();
            swWriter.Close();
        }
        // збереження нового обраного шрифту
        private void AddFontToArray()
        {

            HelpfulFunction.ListAllChangeFonts.Add(new FontTypeData()
            {
                Position = itemText.SelectionStart,
                FontName = toolStripShifts.Text,
                FontSize = toolStripShiftSize.Text,
                FontType = FontStyleSave,
                ColorName = toolStripColor.BackColor.Name
            }

            );
            if (itemText.SelectedText != String.Empty || itemText.SelectionStart < itemText.Text.Length)
            {
                FontTypeData data; 
                int secondPosition;
                    if(itemText.SelectedText == String.Empty)
                    {
                        data = HelpfulFunction.GetNextFontTypeInsertedText();
                        secondPosition = itemText.SelectionStart + 1;
                    }
                    else{
                        data = HelpfulFunction.ListAllChangeFonts[HelpfulFunction.ListAllChangeFonts.Count-2];
                        secondPosition = itemText.SelectionStart + itemText.SelectedText.Length;
                    }
                HelpfulFunction.ListAllChangeFonts.Add(new FontTypeData() 
                {
                    FontName = data.FontName,
                    FontSize = data.FontSize,
                    FontType = data.FontType,
                    Position = secondPosition,
                    ColorName = data.ColorName
                }
                );
            }

        }
        // функція зміни шрифту (жирного, курсивного, підкресленого)
        private Font ChangeFonts(/*FontStyle font*/)
        {
            switch (/*font*/FontStyleSave)
            {
                case /*FontStyle.Underline*/"Underline":
                    {
                        toolStripButton3.BackColor = SystemColors.ActiveBorder;
                        toolStripButton1.BackColor = toolStripButton2.BackColor = SystemColors.Control;
                       
                        AddFontToArray();
                        return new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text), FontStyle.Underline);

                    }
                case /*FontStyle.Bold*/"Bold":
                    {
                        toolStripButton1.BackColor = SystemColors.ActiveBorder;
                        toolStripButton3.BackColor = toolStripButton2.BackColor = SystemColors.Control;
                      //  FontStyleSave = "Bold";
                        AddFontToArray();
                        return new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text), FontStyle.Bold);
                    }
                case /*FontStyle.Italic*/ "Italic":
                    {
                        toolStripButton2.BackColor = SystemColors.ActiveBorder;
                        toolStripButton3.BackColor = toolStripButton1.BackColor = SystemColors.Control;
                      //  FontStyleSave = "Italic";
                        AddFontToArray();
                        return new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text), FontStyle.Italic);

                    }
                default:
                    //FontStyleSave = "Regular";
                    AddFontToArray();
                    return new Font(toolStripShifts.Text, float.Parse(toolStripShiftSize.Text));
            }
        }
        #endregion
      
        private void itemText_SelectionChanged(object sender, EventArgs e)
        {
           
            ChangeCursorOrSelectionText();
        }
        
        private void itemText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 9)
                e.Handled = true;
        }
        // функція збереження адрес зображень
        private List<string> addrImages = new List<string>();
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    if ((Image)new Bitmap(openFileDialog1.FileName) is Image)
                    {
                        addrImages.Add(openFileDialog1.FileName);
                       
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Помилка при відкритті файла. Оберіть будь-ласка інший файл");
                }
            }
        }
    }
}
