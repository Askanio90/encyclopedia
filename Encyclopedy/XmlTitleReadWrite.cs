﻿
using System.Collections.Generic;

using System.Xml;
using System.IO;
using System.Windows.Forms;

namespace Encyclopedy
{
    class XmlTitleReadWrite // клас для роботи з xml файлом
    {
        private string pathToXmlFile;
        private TitleData selectedTitleData;

        public XmlTitleReadWrite(string fullPath) // створення файлу під час першого використання
        {
            pathToXmlFile = fullPath;
            if (!File.Exists(@pathToXmlFile))
            {
                XmlDocument document = new XmlDocument();
                document.CreateXmlDeclaration("1.0", "utf-8", null);
                XmlNode root = document.CreateElement("Titles");
                document.AppendChild(root);
                document.Save(@pathToXmlFile);
            }
        }

        public TitleData SelectedTitleData
        {
            get
            {
                return selectedTitleData;
            }
        }

        public void Write(TitleData data) // запис інформації у файл про взаємозвязки між файлами та ключовими словами
        {
            XmlDocument document = new XmlDocument();
            document.Load(@pathToXmlFile);

            data.TitleId = GetNewIdTitle(document);
            XmlNode qw = document.CreateElement("message");
            qw.InnerText = data.FileName;
         
            XmlNode root = document.DocumentElement;
           
            root.AppendChild(qw);
            XmlAttribute attribute = document.CreateAttribute("IdTitle");
            
            attribute.Value = data.TitleId.ToString();                 
            
            qw.Attributes.Append(attribute);
            attribute = document.CreateAttribute("IdHead");
            attribute.Value = data.HeadId;
            qw.Attributes.Append(attribute);
            attribute = document.CreateAttribute("keywords");
            attribute.Value = data.Keywords;
            qw.Attributes.Append(attribute);
            document.Save(@pathToXmlFile);

            HelpfulFunction.SelectedTitleData = data.Copy();
        }

        // функція перейменування ключових слів
        public void RenameTitle(string lastName, string newName)
        {
            XmlDocument document = new XmlDocument();
            document.Load(@pathToXmlFile);

            var allNodes = document.FirstChild;
            foreach (XmlNode value in allNodes)
            {
                if (value.Attributes["keywords"] == null)
                    continue;
                if (value.Attributes["keywords"].Value == lastName)
                    {
                        value.Attributes["keywords"].Value = newName;
                        break;
                    }
            }

            document.Save(@pathToXmlFile);
        }

        
        public void RemoveAllTitlesInTreeNode(TreeNode deleteNode)
        {
            XmlDocument document = new XmlDocument();
            document.Load(@pathToXmlFile);
            DeleteTitleAndArticle(ref document, deleteNode);
  
            document.Save(@pathToXmlFile);
        
        }


        // видалення з файла назви статтей, що видаляються, а також файлів з їх змістом та зображень
        private void DeleteTitleAndArticle(ref XmlDocument document, TreeNode deleteNode)
        {
                var allNode = document.FirstChild;
                foreach (XmlNode xmlNode in allNode)
                {
                    if (xmlNode.Attributes["IdHead"] == null)
                        continue;
                       if (xmlNode.Attributes["IdHead"].Value == deleteNode.Name)
                        {
                            string fileNameArticle = this.pathToXmlFile.Substring(0, pathToXmlFile.LastIndexOf(@"\"));
                            fileNameArticle += @"\Articles\";
                            if (File.Exists(@fileNameArticle+xmlNode.InnerText))
                            {
                                File.Delete(@fileNameArticle+xmlNode.InnerText);
                            }
                            fileNameArticle += xmlNode.Attributes["IdTitle"].Value;
                            if(Directory.Exists(@fileNameArticle ))
                                Directory.Delete(@fileNameArticle, true);

                            xmlNode.RemoveAll();
                            break;
                        }
                }
            foreach(TreeNode value in deleteNode.Nodes)
                DeleteTitleAndArticle(ref document, value);
            
        }

        // пошук нового ідентифікатора для ключових слів
        private int GetNewIdTitle(XmlDocument document)
        {
            int newId = 1;
            XmlNode lastNode = document.LastChild.LastChild;////?????  neeed check if first title don't be error !!!!!
            if (lastNode == null)
                return newId;

            if (lastNode.Attributes["IdTitle"] != null)
            {
                 newId = int.Parse(lastNode.Attributes["IdTitle"].Value) + 1;
                 return newId;
            }
            
            return newId;
        }

        // функція пошуку файла із змістом статті відповідно до ключових слів
        public string GetFileNameOfArticle(string keywords/*titleId*/)
        {
            
            XmlDocument document = new XmlDocument();
            document.Load(@pathToXmlFile);

            var allNodes = document.FirstChild;
            foreach (XmlNode value in allNodes)
            {
                if (value.Attributes["keywords"] == null)
                    continue;
                if (value.Attributes["keywords"].Value == keywords)
                {
                    selectedTitleData = new TitleData() 
                    {
                        HeadId = value.Attributes["IdHead"].Value,
                        TitleId = int.Parse(value.Attributes["IdTitle"].Value),
                        Keywords = value.Attributes["keywords"].Value,
                        FileName = value.InnerText
                    };
                    return value.InnerText;
                }
            }

            document.Save(@pathToXmlFile);
            return null;
        }

        // функція отримання усіх ключових слів із файла
        public List<string> GetAllKeywords()
        {
            List<string> allKeywords = new List<string>();
            XmlDocument document = new XmlDocument();
            document.Load(@pathToXmlFile);
            var allNodes = document.FirstChild;
            foreach (XmlNode value in allNodes)
            {
                if (value.Attributes["keywords"] == null)
                    continue;
                allKeywords.Add(value.Attributes["keywords"].Value);
            }
            return allKeywords;
        }
    }
}
