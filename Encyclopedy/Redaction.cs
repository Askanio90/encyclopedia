﻿using System;

using System.Windows.Forms;

namespace Encyclopedy
{
    public partial class Redaction : Form
    {
        public Redaction()
        {
            InitializeComponent();
        }

        public string LabelText { get; set; }

        public string Result { get; set; }//властивість, що повертає нове імя розділу
        

        private void Redaction_Load(object sender, EventArgs e)
        {
            label1.Text = LabelText;
            textBox1.Text = Result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == String.Empty)
                MessageBox.Show("Введіть, будь-ласка назву нового розділу");
            else
            {
                Result = textBox1.Text;
               this.DialogResult = DialogResult.OK;
            }
        }
    }
}
