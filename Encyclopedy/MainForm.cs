﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace Encyclopedy
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        // функція створення нової статті
        private void додатиНовуСтаттюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckPassword checkPassword = new CheckPassword();
            if(checkPassword.ShowDialog() == DialogResult.OK)
            {
                checkPassword.Close();

                NewArticle na = new NewArticle();

                na.Action = "Create";
                if (na.ShowDialog() == DialogResult.OK)
                    MainForm_Load(new Object(), new EventArgs());
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string path = Application.StartupPath;
            path += @"\Configuration";
            if (!Directory.Exists(@path))
                Directory.CreateDirectory(@path);
            if (!File.Exists(@path + @"\Config.bin"))
            {
                // виклик вікна для створення пароля
                CreatePassword createPassword = new CreatePassword();
                createPassword.First = true;
                if (createPassword.ShowDialog() != DialogResult.OK)
                {
                    MessageBox.Show("Необхідно обов'язково створити пароль");
                    Close();
                }
            }
            treeView1.Nodes.Clear();
           // articleText.Clear();
            HelpfulFunction.TreeViewListAdd(treeView1);
        }

        private string[] imagesName = null;
        private int imageIndex = 0;

        //функція відновлення інформації статті із файла
        private void AddArticleFromFileToRichEdit(string path, string fileOpen)
        {
            if (fileOpen == null)
                return;

            path = path.Substring(0, path.LastIndexOf(@"\"));
            path += @"\Articles";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            fileOpen = path + @"\" + fileOpen;
            if (!File.Exists(@fileOpen))
                return;

            articleText.Clear();

            StreamReader strRead = new StreamReader(fileOpen);
            HelpfulFunction.RecoverArticleFromFile(strRead.ReadToEnd(), articleText);
            
            strRead.Close();
            path += @"\" + HelpfulFunction.SelectedTitleData.TitleId.ToString();
            if (Directory.Exists(@path)) // відображення зображень до статті
            {
                

                imagesName = Directory.GetFiles(@path);
                imageIndex = 0;
                if (imagesName.Length != 0)
                {
                    pictureBox1.Image = (Image) new Bitmap(imagesName[imageIndex] /*@path + @"\" + imageName*/);
                    pictureBox1.ClientSize = new System.Drawing.Size(300, 300);

                    if (!pictureBox1.Visible)
                    {
                        Width = splitContainer1.Width + 400;
                    }
                    articleText.Dock = DockStyle.Left;
                    pictureBox1.Visible = true;
                    pictureBox1.Dock = DockStyle.Top;
                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                    if (imagesName.Length > 1)
                    {
                        timerNextImage.Enabled = true;
                        imageIndex++;
                    }
                    else if (timerNextImage.Enabled)
                        timerNextImage.Enabled = false;
                }
            }
            else if(pictureBox1.Visible)
            {
                pictureBox1.Visible = false;
                pictureBox1.Dock = DockStyle.None;
                articleText.Dock = DockStyle.Fill;
                timerNextImage.Enabled = false;
                Width -= 400;
            }
        }
        // функція відображення інформації статті після вибору зі змісту
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode == null)
                return;

            string articleName = treeView1.SelectedNode.Text;
            string path = Application.StartupPath + @"\Configuration\Titles.xml";
            XmlTitleReadWrite xml = new XmlTitleReadWrite(path);
            string fileOpen = xml.GetFileNameOfArticle(articleName);
            HelpfulFunction.SelectedTitleData = xml.SelectedTitleData; // збереження даних про обрану статтю
            if (fileOpen != null)
            {
                AddArticleFromFileToRichEdit(path, fileOpen);
                
            }
            this.Text = treeView1.SelectedNode.Text;
    
       }
        // внесення усіх ключових слів у поле для пошуку по алфавіту
        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {

            if (e.TabPage.Name != "tabPage2")
                return;

            string path = Application.StartupPath + @"\Configuration\Titles.xml";
            XmlTitleReadWrite xml = new XmlTitleReadWrite(@path);
            List<string> allKeywords = xml.GetAllKeywords();
            listBox1.Items.Clear();
            foreach (string value in allKeywords)
            {
                listBox1.Items.Add(value);
            }
        }
        // пошук по імені ключових слів
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.Items.Count == 0)
                return;

            foreach(string value in listBox1.Items)
            {
            
                if (value.CompareTo(textBox1.Text) >= 0)
                {
                    listBox1.SelectedItem = value;
                    break;
                }
            }
        }
        // пошук інформації під час пошуку по ключових словах
        private void listBox1_Click(object sender, EventArgs e)
        {
            string keywords = listBox1.Items[listBox1.SelectedIndex].ToString();
            string path = Application.StartupPath + @"\Configuration\Titles.xml";
            XmlTitleReadWrite xml = new XmlTitleReadWrite(path);
            string fileOpen = xml.GetFileNameOfArticle(keywords);
            HelpfulFunction.SelectedTitleData = xml.SelectedTitleData;
            if (fileOpen != null)
            {
                AddArticleFromFileToRichEdit(path, fileOpen);
                this.Text = HelpfulFunction.SelectedTitleData.Keywords;
            }

        }

        private void редагуватиСтаттюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckPassword checkPassword = new CheckPassword();
            if(checkPassword.ShowDialog() == DialogResult.OK)
            {
                checkPassword.Close();

                NewArticle na = new NewArticle();
                na.Action = "Replace";

                if (na.ShowDialog() == DialogResult.OK)
                    MainForm_Load(new Object(), new EventArgs());
            }
        }

        private void видалитиСтаттюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckPassword checkPassword = new CheckPassword();
            if(checkPassword.ShowDialog() == DialogResult.OK)
            {
                checkPassword.Close();

                NewArticle na = new NewArticle();
                na.Action = "Delete";
                if (na.ShowDialog() == DialogResult.OK)
                    MainForm_Load(new Object(), new EventArgs());
            }
        }

        private void проПрограмуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 ab = new AboutBox1();
            ab.ShowDialog();
        }


        // зміна пароля
        private void друкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckPassword checkPassword = new CheckPassword();
            if (checkPassword.ShowDialog() == DialogResult.OK)
            {
                checkPassword.Close();

                CreatePassword createPassword = new CreatePassword();
                createPassword.First = false;
                createPassword.ShowDialog();
            }
        }
        // функція зміни зображень для статті
        private void timerNextImage_Tick(object sender, EventArgs e)
        {
            if (imageIndex < imagesName.Length)
            {
                pictureBox1.Image = new Bitmap(imagesName[imageIndex]);
                imageIndex++;
            }
            else
            {
                imageIndex = 0;
            }
        }

        private void допомогаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Instruction instruction = new Instruction();
            instruction.ShowDialog();
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
   
    }
}
