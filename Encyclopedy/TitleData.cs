﻿using System;


namespace Encyclopedy
{
    class TitleData // клас-модель xml файла
    {
        public int TitleId { get; set; }
        public string HeadId { get; set; }
        public string Keywords { get; set; }
        public string FileName { get; set; }

        public TitleData Copy()
        {
            return new TitleData() 
            {
                TitleId = this.TitleId,
                HeadId = this.HeadId,
                Keywords = this.Keywords, 
                FileName = this.FileName
            };
        }
    }

    class FontTypeData : IComparable // клас для збереження виду шрифта 
    {
         public int Position { get; set; }
         public string FontName { get; set; }
         public string FontSize { get; set; }
         public string FontType { get; set; }
         public string ColorName { get; set; }

         public FontTypeData Copy()
         {
             return new FontTypeData() 
             {
                 Position = this.Position,
                 FontName = this.FontName,
                 FontSize = this.FontSize,
                 FontType = this.FontType, 
                 ColorName = this.ColorName
             };
         }

         public int CompareTo(object compareObject) // реалізація інтерфейса для можливості порівняння обєктів через їх позицію
         {
             if (compareObject is FontTypeData)
             {
                 FontTypeData obj = (FontTypeData) compareObject;
                 if (Position == obj.Position)
                     return 0;
                 if (Position > obj.Position)
                     return 1;
                     
                 return -1;
            }
             return 0;
         }
    }
}
