﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Encyclopedy
{
    public partial class CreatePassword : Form
    {
        public CreatePassword()
        {
            InitializeComponent();
        }

        public bool First { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            if (maskedTextBox1.Text == String.Empty || maskedTextBox2.Text == String.Empty || richTextBox1.Text == String.Empty)
            {
                MessageBox.Show("Заповніть будь-ласка всі поля");
                return;
            }
            if (maskedTextBox1.Text != maskedTextBox2.Text)
            {
                MessageBox.Show("Значення полів пароля та підтвердження пароля не співпадають");
                return;
            }
            char[] userPassword = maskedTextBox1.Text.ToCharArray();
            // слово для шифрування пароля
            char[] mashineWord = {'s', 'o', 'm', 'y', 'c', 'o', 'd','e', 'w', 'o', 'r', 'd'};
            int k = 0;
            int[] codePassword = new int[userPassword.Length];
            //шифрування пароля
            for (int i = 0; i < userPassword.Length; i++)
            {
                if (k >= mashineWord.Length)
                    k = 0;
                codePassword[i] = userPassword[i] + mashineWord[k];
                k++;
            }
            string path = Application.StartupPath;
            path += @"\Configuration";
            if (!Directory.Exists(@path))
                Directory.CreateDirectory(@path);

            path += @"\Config.bin";
            StreamWriter swWriter = new StreamWriter(@path);
            for(int i = 0; i < codePassword.Length; i++)
                swWriter.WriteLine(codePassword[i]);

            swWriter.WriteLine("###");
            swWriter.WriteLine(richTextBox1.Text);
            swWriter.Close();

            MessageBox.Show("Пароль успішно збережено!!!!");
            DialogResult = DialogResult.OK;
        }

        private void CreatePassword_Load(object sender, EventArgs e)
        {
            if (!First)
                label4.Visible = label5.Visible = false;
        }
    }
}
