﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Encyclopedy
{
    public partial class CheckPassword : Form
    {
        public CheckPassword()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (maskedTextBox1.Text == String.Empty)
            {
                MessageBox.Show("Введіть, будь-ласка, пароль у відповідне поле");
                return;
            }
           
            string path = CheckFilePassword(); 
            if (path!=null)
            {
               
                StreamReader strReader = new StreamReader(@path);
               // ключове слово що використовується для дешифрування
                char[] machineWord = { 's', 'o', 'm', 'y', 'c', 'o', 'd', 'e', 'w', 'o', 'r', 'd' };
                int k = 0;
                List<int> decryptPass = new List<int>();
                // зчитування шифрованого пароля з файла та його дешифрація
                while (!strReader.EndOfStream)
                {

                    if (k >= machineWord.Length)
                        k = 0;
                    string pass = strReader.ReadLine();
                    if (pass == "###")
                        break;
                    decryptPass.Add(int.Parse(pass) - machineWord[k]);
                    k++;
                }
                char[] decrypt = new char[decryptPass.Count];
                for (int i = 0; i < decryptPass.Count; i++)
                    decrypt[i] = (Char) decryptPass[i];

                if (maskedTextBox1.Text != new string(decrypt))
                {
                    MessageBox.Show("Невірний пароль");
                    maskedTextBox1.Clear();
                    return;
                }
                strReader.Close();
                DialogResult = DialogResult.OK;
            }
        }
        // перевірка існування файла з паролем
        private string CheckFilePassword()
        {
            string path = Application.StartupPath;
            path += @"\Configuration";
            if (Directory.Exists(@path))
            {
                path += @"\Config.bin";
                if (!File.Exists(@path))
                {
                    MessageBox.Show("Файл із паролем був пошкоджений. Будь-ласка для початку створіть пароль");
                    return null;
                }
            }
            else
            {
                return null;
            }
            return path;
        }
        // відображення підказки
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox1.Checked == true)
            {
                string path = CheckFilePassword();
                if(path == null)
                    return;
                StreamReader streamReader = new StreamReader(@path);
                
                string text = streamReader.ReadToEnd();
                text = text.Substring(text.IndexOf("###") + 3);
                streamReader.Close();
                toolTip1.SetToolTip(this, text);
                toolTip1.Active = true;
            }
            else
            {
                toolTip1.Active = false;
            }
        }
    }
}
